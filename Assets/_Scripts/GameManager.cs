﻿using System.Collections;
using System.Collections.Generic;
using TrapsManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // static
    public static GameManager Instance;

    // referenses
    public EnemySpawner spawner;
    public MapGenerator mapGenerator;
    public MainBaseBehaviour mainBasePrefab;

    // parameters
    public int startingResources = 200;

    // cache
    private GridTile mainBaseTile;



    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        spawner.StartSpawning();
        mainBaseTile = mapGenerator.GetMainBaseTile();
        SpawnBase();
        TrapsBuilder.AddResources(startingResources);
    }

    public GridTile GetMainBaseTile()
    {
        return mainBaseTile;
    }

    private void SpawnBase()
    {
        MainBaseBehaviour mainBase = Instantiate(mainBasePrefab, MapGenerator.GetPosition(mainBaseTile) + Vector3.up, Quaternion.identity);
    }
}