﻿namespace TrapsManagement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Traps/Recipe", fileName = "Trap_")]
    public class TrapRecipe : ScriptableObject
    {
        // referenses
        public Trap resultingTrap;
        public Sprite sprite;

        // parameters
        public string trapName;
        [TextArea]
        public string description;
        public int cost;
    }
}