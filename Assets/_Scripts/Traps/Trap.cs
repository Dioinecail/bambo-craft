﻿namespace TrapsManagement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class Trap : MonoBehaviour
    {
        // parameters
        public float baseDamage;
        public float baseAttackSpeed;
        public float baseAttackRadius;

        [HideInInspector]
        public float damage;
        [HideInInspector]
        public float attackSpeed;
        [HideInInspector]
        public float attackRadius;

        public LayerMask attackMask;

        // cache
        protected Coroutine coroutine_attack;
        protected Collider[] attackBuffer = new Collider[32];

        public UnityEvent onAttackEvent;



        protected void Awake()
        {
            damage = baseDamage;
            attackSpeed = baseAttackSpeed;
            attackRadius = baseAttackRadius;

            coroutine_attack = StartCoroutine(AttackCoroutine());
        }

        public virtual void AttackAction()
        {
            onAttackEvent?.Invoke();
        }

        public virtual void UpgradeTrap()
        {
            // TODO: Upgrade logic
        }

        protected void ClearBuffer()
        {
            for (int i = 0; i < attackBuffer.Length; i++)
            {
                attackBuffer[i] = null;
            }
        }

        protected IEnumerator AttackCoroutine()
        {
            yield return new WaitForSeconds(1 / attackSpeed);

            AttackAction();

            yield return AttackCoroutine();
        }

        protected void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, baseAttackRadius);
        }
    }
}