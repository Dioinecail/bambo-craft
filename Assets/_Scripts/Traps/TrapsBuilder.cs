﻿namespace TrapsManagement
{
    using System;
    using UnityEngine;

    public class TrapsBuilder : MonoBehaviour
    {
        // static
        public static event Action<int> onResourcesChanged;
        public static event Action<TrapRecipe> onTrapSelected;
        public static int availableResources;

        // referenses
        public RaycasterBehaviour raycasterBehaviour;
        public Material previewMaterialAvailable;
        public Material previewMaterialUnavailable;

        public TrapRecipe[] recipes;

        // cache
        private GameObject preview;
        private Renderer[] previewRenderers;
        private int selectedTrapIndex;



        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
                SelectTrap(-1);

            if (Input.GetKeyDown(KeyCode.E))
                SelectTrap(1);

            ShowPreview(raycasterBehaviour.GetCurrentTile());

            if (Input.GetMouseButtonDown(0))
            {
                PlaceTrap(raycasterBehaviour.GetCurrentTile());
            }
        }

        private void SelectTrap(int direction)
        {
            selectedTrapIndex += direction;

            if (selectedTrapIndex >= recipes.Length)
                selectedTrapIndex = 0;
            else if (selectedTrapIndex < 0)
                selectedTrapIndex = recipes.Length - 1;

            SetNewPreview(recipes[selectedTrapIndex].resultingTrap.gameObject);

            onTrapSelected?.Invoke(recipes[selectedTrapIndex]);
        }

        private void PlaceTrap(TileObject tile)
        {
            if (CanBuild(selectedTrapIndex) && tile != null)
            {
                Trap newTrap = BuildTrap(selectedTrapIndex);
                newTrap.transform.position = tile.transform.position + new Vector3(0, 1, 0);
            }
            else
            {
                return;
            }
        }

        private bool CanBuild(int index)
        {
            return recipes[index].cost <= availableResources;
        }

        private Trap BuildTrap(int index)
        {
            AddResources(-recipes[index].cost);
            UpdatePreview();
            return Instantiate(recipes[index].resultingTrap);
        }

        private void ShowPreview(TileObject tile)
        {
            if (tile != null)
            {
                if (preview != null)
                {
                    preview.SetActive(true);
                    preview.transform.position = tile.transform.position + new Vector3(0, 1, 0);
                }
            }
            else
            {
                if (preview != null)
                    preview.SetActive(false);
            }
        }

        private void SetNewPreview(GameObject previewPrefab)
        {
            if (preview != null)
                Destroy(preview);

            preview = Instantiate(previewPrefab);

            MonoBehaviour[] components = preview.GetComponentsInChildren<MonoBehaviour>();

            foreach (var comp in components)
            {
                Destroy(comp);
            }

            previewRenderers = preview.GetComponentsInChildren<MeshRenderer>();

            UpdatePreview();
        }

        private void UpdatePreview()
        {
            if(previewRenderers != null)
            {
                SetMaterials(previewRenderers, (recipes[selectedTrapIndex].cost <= availableResources) 
                    ? previewMaterialAvailable : previewMaterialUnavailable);
            }
        }

        private void SetMaterials(Renderer[] renderers, Material mat)
        {
            foreach (var r in renderers)
            {
                Material[] mats = new Material[r.sharedMaterials.Length];
                for (int i = 0; i < mats.Length; i++)
                {
                    mats[i] = mat;
                }

                r.sharedMaterials = mats;
            }
        }

        public static void AddResources(int amount)
        {
            availableResources += amount;
            onResourcesChanged?.Invoke(availableResources);
        }
    }
}