﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrapsManagement;

public class SplashTrap : Trap
{
    public override void AttackAction()
    {
        ClearBuffer();

        bool attacked = false;

        int buffer = Physics.OverlapSphereNonAlloc(transform.position, attackRadius, attackBuffer, attackMask);

        for (int i = 0; i < buffer; i++)
        {
            EnemyBehaviour enemy = attackBuffer[i].GetComponent<EnemyBehaviour>();

            if(enemy != null)
            {
                enemy.RecieveDamage(damage);
                attacked = true;
            }
        }

        if(attacked)
            base.AttackAction();
    }
}