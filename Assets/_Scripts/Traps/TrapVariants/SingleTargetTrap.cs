﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrapsManagement;

public class SingleTargetTrap : Trap
{
    public override void AttackAction()
    {
        ClearBuffer();

        int buffer = Physics.OverlapSphereNonAlloc(transform.position, attackRadius, attackBuffer, attackMask);

        EnemyBehaviour enemy = null;
        float distance = 10000;

        for (int i = 0; i < buffer; i++)
        {
            EnemyBehaviour checkEnemy = attackBuffer[i].GetComponent<EnemyBehaviour>();

            if(checkEnemy != null)
            {
                if(Vector3.Distance(transform.position, checkEnemy.transform.position) < distance)
                {
                    enemy = checkEnemy;
                    distance = Vector3.Distance(transform.position, enemy.transform.position);
                }
            }
        }

        if (enemy != null)
        {
            enemy.RecieveDamage(damage);
            base.AttackAction();
            return;
        }
    }
}