﻿using System.Collections;
using System.Collections.Generic;
using TrapsManagement;
using UnityEngine;

public class SlowingTrap : Trap
{
    public float slowFactor;



    public override void AttackAction()
    {
        ResetEnemySpeed();
        ClearBuffer();

        bool attacked = false;

        int buffer = Physics.OverlapSphereNonAlloc(transform.position, attackRadius, attackBuffer, attackMask);

        for (int i = 0; i < buffer; i++)
        {
            EnemyBehaviour enemy = attackBuffer[i].GetComponent<EnemyBehaviour>();

            if (enemy != null)
            {
                enemy.movementSpeed /= slowFactor;
                attacked = true;
            }
        }

        if(attacked)
            base.AttackAction();
    }

    protected void ResetEnemySpeed()
    {
        for (int i = 0; i < attackBuffer.Length; i++)
        {
            if(attackBuffer[i] != null)
            {
                EnemyBehaviour enemy = attackBuffer[i].GetComponent<EnemyBehaviour>();

                if(enemy != null)
                {
                    enemy.ResetMovementSpeed();
                }
            }
        }
    }
}