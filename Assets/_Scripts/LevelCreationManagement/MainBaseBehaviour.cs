﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MainBaseBehaviour : MonoBehaviour
{
    // static
    public static MainBaseBehaviour Instance;
    public static Action<float> onHealthChanged;

    // parameters
    public int health;
    public int maxHealth = 20;

    // cache
    public new Transform transform;



    private void Awake()
    {
        Instance = this;
        transform = GetComponent<Transform>();
    }

    public void RecieveDamage()
    {
        health--;

        onHealthChanged?.Invoke((float)health / maxHealth);
    }
}