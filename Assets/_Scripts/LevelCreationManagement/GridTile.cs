﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GridTile : IHeapItem<GridTile>
{
    public int X;
    public int Y;

    public Vector2 position;

    public bool Wall;

    public int gCost;
    public int hCost;

    public GridTile pathParent;
    int heapIndex;



    public GridTile()
    {
        
    }

    public GridTile (Vector2 position, bool _wall)
    {
        Wall = _wall;
        X = Mathf.RoundToInt(position.x);
        Y = Mathf.RoundToInt(position.y);

        this.position = position;
    }

    public  GridTile (int _x, int _y, bool _wall)
    {
        X = _x;
        Y = _y;
        Wall = _wall;
        position = new Vector2(_x, _y);
    }

    public int fCost
    {
        get
        {
            return gCost = hCost;
        }
    }

    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }

        set
        {
            heapIndex = value;
        }
    }

    public int CompareTo(GridTile other)
    {
        int compare = fCost.CompareTo(other.fCost);
        if(compare == 0)
        {
            compare = hCost.CompareTo(other.hCost);
        }
        return -compare;
    }

    public override string ToString()
    {
        return string.Format("[{0}:{1}]", X, Y);
    }
}
