﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorEditor : Editor
{
    private MapGenerator generator;

    private void OnEnable()
    {
        generator = (MapGenerator)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();



        if(GUILayout.Button("Generate", EditorStyles.miniButtonRight))
        {
            generator.GenerateMap();
        }
    }
}