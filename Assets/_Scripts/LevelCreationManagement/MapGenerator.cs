﻿using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour 
{
    public static float tilesScale = 2;

    // parameters
    public int sizeX;
    public int sizeY;
    public int borderSize = 5;
    public int iterationCount = 3;

    [Range(0, 100)]
    public float RandomFillPercent;

    public int UpperWallCountLimit;
    public int LowerWallCountLimit;

    // cache
    public int MaxGridSize
    {
        get; private set;
    }
    private Color[] roomColors;
    public GridTile[,] Map;



    private void Awake()
    {
        MaxGridSize = sizeX * sizeY;
        GenerateMap();
    }

    private void OnDrawGizmos()
    {
        if (Map == null)
            return;

        for (int x = 0; x < Map.GetLength(0); x++)
        {
            for (int y = 0; y < Map.GetLength(1); y++)
            {
                switch (Map[x, y].Wall)
                {
                    case true:
                        Gizmos.color = Color.black;
                        break;
                    default:
                        Gizmos.color = Color.white;
                        break;
                }

                Gizmos.DrawCube(new Vector3(x, y), Vector3.one);
            }
        }
    }


    #region MAP PROCESSING



    public void GenerateMap()
    {
        Map = new GridTile[sizeX + borderSize * 2, sizeY + borderSize * 2];

        for (int x = 0; x < sizeX + borderSize * 2; x++)
        {
            for (int y = 0; y < sizeY + borderSize * 2; y++)
            {
                bool wall = false;
                if (x < borderSize - 1 || x > sizeX + borderSize || y < borderSize - 1 || y > sizeY + borderSize)
                    wall = true;
                else
                    wall = (UnityEngine.Random.Range(0, 101) < RandomFillPercent);
                Map[x, y] = new GridTile(x, y, wall);
            }
        }
        for (int i = 0; i < iterationCount; i++)
        {
			ProcessMap();
        }

        List<List<GridTile>> rooms = GetRegions();

        List<Room> roomsList = new List<Room>();

        roomColors = new Color[rooms.Count];
        for (int i = 0; i < rooms.Count; i++)
        {
            roomsList.Add(new Room(rooms[i], Map));
            roomColors[i] = new Color(UnityEngine.Random.Range(0.5f, 1), UnityEngine.Random.Range(0.5f, 1), UnityEngine.Random.Range(0.5f, 1), 1);
        }
        roomsList.Sort();
        roomsList[0].isMainRoom = true;
        roomsList[0].isAccessibleFromMainRoom = true;

		ConnectClosestRooms(roomsList);

        //foreach (Room room in allRooms)
        //{
        //    foreach (GridTile tile in room.tiles)
        //    {
        //        room.tileSprites.Add(GenerateTile(tile));
        //    }
        //}

        //foreach (GridTile tile in Map)
        //{
        //    if (tile.Wall)
        //        GenerateTile(tile);
        //}

        //for (int i = 0; i < allRooms.Length; i++)
        //{
        //    for (int j = 0; j < allRooms[i].tileSprites.Count; j++)
        //    {
        //        allRooms[i].tileSprites[j].color = roomColors[i];
        //    }
        //}
    }

    private void ProcessMap()
    {
        for (int x = 0; x < Map.GetLength(0); x++)
        {
            for (int y = 0; y < Map.GetLength(1); y++)
            {
                if (GetNeighbourWallCount(x, y) > UpperWallCountLimit)
                    Map[x, y].Wall = true;
                else if (GetNeighbourWallCount(x, y) < LowerWallCountLimit)
                    Map[x, y].Wall = false;
            }
        }
    }

    private void ConnectClosestRooms(List<Room> roomsToConnect, bool forceAccessibilityFromMainRoom = false)
	{
		List<Room> roomListA = new List<Room>();
		List<Room> roomListB = new List<Room>();

		if (forceAccessibilityFromMainRoom)
		{
			foreach (Room room in roomsToConnect)
			{
				if (room.isAccessibleFromMainRoom)
				{
					roomListB.Add(room);
				}
				else
				{
					roomListA.Add(room);
				}
			}
		}
		else
		{
			roomListA = roomsToConnect;
			roomListB = roomsToConnect;
		}

		int bestDistance = 0;
        GridTile bestTileA = new GridTile();
        GridTile bestTileB = new GridTile();
		Room bestRoomA = new Room();
		Room bestRoomB = new Room();
		bool possibleConnectionFound = false;

		foreach (Room roomA in roomListA)
		{
			if (!forceAccessibilityFromMainRoom)
			{
				possibleConnectionFound = false;
				if (roomA.connectedRooms.Count > 0)
				{
					continue;
				}
			}

			foreach (Room roomB in roomListB)
			{
				if (roomA == roomB || roomA.IsConnected(roomB))
				{
					continue;
				}

				for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; tileIndexA++)
				{
					for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; tileIndexB++)
					{
						GridTile tileA = roomA.edgeTiles[tileIndexA];
						GridTile tileB = roomB.edgeTiles[tileIndexB];
						int distanceBetweenRooms = (int)(Mathf.Pow(tileA.X - tileB.X, 2) + Mathf.Pow(tileA.Y - tileB.Y, 2));

						if (distanceBetweenRooms < bestDistance || !possibleConnectionFound)
						{
							bestDistance = distanceBetweenRooms;
							possibleConnectionFound = true;
							bestTileA = tileA;
							bestTileB = tileB;
							bestRoomA = roomA;
							bestRoomB = roomB;
						}
					}
				}
			}
			if (possibleConnectionFound && !forceAccessibilityFromMainRoom)
			{
				CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
			}
		}

		if (possibleConnectionFound && forceAccessibilityFromMainRoom)
		{
			CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
			ConnectClosestRooms(roomsToConnect, true);
		}

		if (!forceAccessibilityFromMainRoom)
		{
			ConnectClosestRooms(roomsToConnect, true);
		}
	}

	private void CreatePassage(Room roomA, Room roomB, GridTile tileA, GridTile tileB)
	{
		Room.ConnectRooms(roomA, roomB);
		//Debug.DrawLine (CoordToWorldPoint (tileA), CoordToWorldPoint (tileB), Color.green, 100);

		List<GridTile> line = GetLine(tileA, tileB);
		foreach (GridTile c in line)
		{
			DrawCircle(c, 0);
		}
	}

    private void DrawCircle(GridTile c, int r)
	{
		for (int x = -r; x <= r; x++)
		{
			for (int y = -r; y <= r; y++)
			{
				if (x * x + y * y <= r * r)
				{
					int drawX = c.X + x;
					int drawY = c.Y + y;
					if (isInsideMap(drawX, drawY))
					{
                        Map[drawX, drawY].Wall = false;
					}
				}
			}
		}
	}

    private List<GridTile> GetLine(GridTile from, GridTile to)
	{
		List<GridTile> line = new List<GridTile>();

		int x = from.X;
		int y = from.Y;

		int dx = to.X - from.X;
		int dy = to.Y - from.Y;

		bool inverted = false;
		int step = Math.Sign(dx);
		int gradientStep = Math.Sign(dy);

		int longest = Mathf.Abs(dx);
		int shortest = Mathf.Abs(dy);

		if (longest < shortest)
		{
			inverted = true;
			longest = Mathf.Abs(dy);
			shortest = Mathf.Abs(dx);

			step = Math.Sign(dy);
			gradientStep = Math.Sign(dx);
		}

		int gradientAccumulation = longest / 2;
		for (int i = 0; i < longest; i++)
		{
			line.Add(Map[x,y]);

			if (inverted)
			{
				y += step;
			}
			else
			{
				x += step;
			}

			gradientAccumulation += shortest;
			if (gradientAccumulation >= longest)
			{
				if (inverted)
				{
					x += gradientStep;
				}
				else
				{
					y += gradientStep;
				}
				gradientAccumulation -= longest;
			}
		}

		return line;
	}



    #endregion

    #region GETTING DATA



    public GridTile GetGridTileFromPosition(Vector2 position)
    {
        int xPos = Mathf.Clamp(Mathf.RoundToInt(position.x), 0, Map.GetLength(0));
        int yPos = Mathf.Clamp(Mathf.RoundToInt(position.y), 0, Map.GetLength(1));

        if (isInsideMap(xPos, yPos))
            return Map[xPos, yPos];
        else
            return null;
    }

    public static Vector3 GetPosition(GridTile tile)
    {
        return new Vector3(tile.X, 0, tile.Y) * tilesScale;
    }

    public List<GridTile> GetNeightbours(GridTile tile)
    {
        List<GridTile> neighbours = new List<GridTile>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                if(isInsideMap(tile.X + x, tile.Y + y))
                {
                    neighbours.Add(Map[tile.X + x, tile.Y + y]);
                }
            }
        }

        return neighbours;
    }

    private int GetNeighbourWallCount(int x, int y)
    {
        int neighbourCount = 0;

        for (int xPos = x - 1; xPos <= x + 1; xPos++)
        {
            for (int yPos =  y - 1; yPos <= y + 1; yPos++)
            {
                if(isInsideMap(xPos, yPos))
                {
                    if(Map[xPos,yPos].Wall)
                    {
                        neighbourCount++;
                    }
                }
                else
                {
                    neighbourCount++;
                }
            }
        }
        return neighbourCount;
    }

    private bool isInsideMap(int x, int y)
    {
        return (x >= 0 && y >= 0 && x < Map.GetLength(0) && y < Map.GetLength(1));
    }

    private List<List<GridTile>> GetRegions()
	{
		List<List<GridTile>> regions = new List<List<GridTile>>();
		int[,] mapFlags = new int[Map.GetLength(0), Map.GetLength(1)];

		for (int x = 0; x < Map.GetLength(0); x++)
		{
			for (int y = 0; y < Map.GetLength(1); y++)
			{
				if (mapFlags[x, y] == 0 && !Map[x,y].Wall)
				{
					List<GridTile> newRegion = GetRegionTiles(x, y);
					regions.Add(newRegion);

					foreach (GridTile tile in newRegion)
					{
						mapFlags[tile.X, tile.Y] = 1;
					}
				}
			}
		}

		return regions;
	}

    private List<GridTile> GetRegionTiles(int startX, int startY)
	{
		List<GridTile> tiles = new List<GridTile>();
		int[,] mapFlags = new int[Map.GetLength(0), Map.GetLength(1)];

		Queue<GridTile> queue = new Queue<GridTile>();
		queue.Enqueue(Map[startX,startY]);
		mapFlags[startX, startY] = 1;

		while (queue.Count > 0)
		{
			GridTile tile = queue.Dequeue();
			tiles.Add(tile);

			for (int x = tile.X - 1; x <= tile.X + 1; x++)
			{
				for (int y = tile.Y - 1; y <= tile.Y + 1; y++)
				{
					if (isInsideMap(x, y) && (y == tile.Y || x == tile.X))
					{
						if (mapFlags[x, y] == 0 && !Map[x, y].Wall)
						{
							mapFlags[x, y] = 1;
							queue.Enqueue(Map[x,y]);
						}
					}
				}
			}
		}
		return tiles;
	}

    public GridTile GetMainBaseTile()
    {
        int startingX = Map.GetLength(0) / 2;
        int startingY = Map.GetLength(1) / 2;

        int currentXIncrement = 0, currentYIncrement = 0;

        GridTile mainBaseTile = new GridTile();
        mainBaseTile.X = -1;

        while(mainBaseTile.X < 0)
        {
            for (int x = startingX - currentXIncrement; x < startingX + currentXIncrement; x++)
            {
                for (int y = startingY - currentYIncrement; y < startingY + currentYIncrement; y++)
                {
                    if (!Map[x, y].Wall)
                        mainBaseTile = Map[x, y];
                }
            }

            currentXIncrement++;
            currentYIncrement++;
        }

        return mainBaseTile;
    }



    #endregion


    [Serializable]
	public class Room : IComparable<Room>
	{
        public List<GridTile> tiles;
        public List<GridTile> edgeTiles;
        public List<GridTile> contentTiles;
        public List<Room> connectedRooms;
        public List<SpriteRenderer> tileSprites;
        public int tileCount;
        public bool isAccessibleFromMainRoom;
		public bool isMainRoom;

        public int contentCount;

		public Room ()
        {
            
        }

		public Room(List<GridTile> roomTiles, GridTile[,] map)
		{
			tiles = roomTiles;
			tileCount = tiles.Count;
			connectedRooms = new List<Room>();

			edgeTiles = new List<GridTile>();
            contentTiles = new List<GridTile>();
            tileSprites = new List<SpriteRenderer>();

			foreach (GridTile tile in tiles)
			{
				for (int x = tile.X - 1; x <= tile.X + 1; x++)
				{
					for (int y = tile.Y - 1; y <= tile.Y + 1; y++)
					{
						if (x == tile.X || y == tile.Y)
						{
                            if (map[x, y].Wall)
							{
								edgeTiles.Add(tile);
							}
						}
					}
				}
			}
		}

		public void SetAccessibleFromMainRoom()
		{
			if (!isAccessibleFromMainRoom)
			{
				isAccessibleFromMainRoom = true;
				foreach (Room connectedRoom in connectedRooms)
				{
					connectedRoom.SetAccessibleFromMainRoom();
				}
			}
		}

		public static void ConnectRooms(Room roomA, Room roomB)
		{
			if (roomA.isAccessibleFromMainRoom)
			{
				roomB.SetAccessibleFromMainRoom();
			}
			else if (roomB.isAccessibleFromMainRoom)
			{
				roomA.SetAccessibleFromMainRoom();
			}
			roomA.connectedRooms.Add(roomB);
			roomB.connectedRooms.Add(roomA);
		}

		public bool IsConnected(Room otherRoom)
		{
			return connectedRooms.Contains(otherRoom);
		}


		public int CompareTo(Room other)
        {
            return other.tileCount.CompareTo(tileCount);
        }
    }
}   