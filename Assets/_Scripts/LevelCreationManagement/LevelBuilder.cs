﻿using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    // referenses
    public Transform levelRoot;
    public MapGenerator mapGenerator;

    public TileObject[] walkableTiles;
    public TileObject[] blockedTiles;

    // parameters
    public bool buildOnAwake;

    // cache
    private List<TileObject> tilesList;



    private void Start()
    {
        if (buildOnAwake)
            BuildLevel();
    }

    private void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //    BuildLevel();
    }

    private void BuildLevel()
    {
        if(tilesList == null)
            tilesList = new List<TileObject>();
        else
        {
            foreach (var tile in tilesList)
            {
                Destroy(tile);
            }
        }

        foreach (var tile in mapGenerator.Map)
        {
            tilesList.Add(CreateTile(tile));
        }

        MakeCollider();
    }

    private TileObject CreateTile(GridTile tile)
    {
        TileObject newTile;

        if (tile.Wall)
            newTile = Instantiate(RandomUtility.GetRandomObject(blockedTiles), levelRoot);
        else
            newTile = Instantiate(RandomUtility.GetRandomObject(walkableTiles), levelRoot);

        newTile.transform.position = MapGenerator.GetPosition(tile);
        newTile.tile = tile;

        return newTile;
    }

    public void MakeCollider()
    {
        float xSize = mapGenerator.Map.GetLength(0);
        float zSize = mapGenerator.Map.GetLength(1);

        Vector3 position = new Vector3(xSize / 2, 0, zSize / 2) * MapGenerator.tilesScale;
        Vector3 size = new Vector3(xSize, 1, zSize) * MapGenerator.tilesScale;

        levelRoot.GetComponent<BoxCollider>().size = size;
        levelRoot.GetComponent<BoxCollider>().center = position;
    }
}