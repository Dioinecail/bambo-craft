﻿using System.Collections;
using System.Collections.Generic;
using TrapsManagement;
using UnityEngine;

public class RaycasterBehaviour : MonoBehaviour
{
    // referenses
    public Transform raycasterTransform;

    // parameters
    public LayerMask raycastMask;

    // cache
    private Vector3 currentPoint;
    private TileObject currentTile;



    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        Raycast();

        if(Input.GetMouseButtonDown(1))
        {
            if (Cursor.lockState == CursorLockMode.Locked)
                Cursor.lockState = CursorLockMode.None;
            else
                Cursor.lockState = CursorLockMode.Locked;
        }
    }

    private void Raycast()
    {
        RaycastHit hit;

        if(Physics.Raycast(raycasterTransform.position, raycasterTransform.forward, out hit, 100, raycastMask))
        {
            if (hit.collider != null)
            {
                // do some action
                currentTile = hit.collider.GetComponent<TileObject>();
            }
            else
                currentTile = null;
        }
        else
            currentTile = null;
    }

    public Vector3 GetCurrentPoint()
    {
        return currentPoint;
    }

    public TileObject GetCurrentTile()
    {
        return currentTile;
    }
}