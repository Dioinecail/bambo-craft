﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // referenses
    public Animator anima;
    public Transform model;

    // parameters
    public float movementSpeed;

    // cache
    private Rigidbody rBody;
    private Vector3 currentMovementInputDirection;
    private Transform movementReferense;



    private void Awake()
    {
        rBody = GetComponent<Rigidbody>();
        movementReferense = FindObjectOfType<Camera>().transform;
    }

    void Update()
    {
        ProcessInputs();    
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void ProcessInputs()
    {
        float xInput = Input.GetAxis("Horizontal");
        float yInput = Input.GetAxis("Vertical");

        Vector3 currentInputDirection = new Vector3(xInput, 0, yInput);
        currentMovementInputDirection = movementReferense.TransformDirection(currentInputDirection);
        currentMovementInputDirection.y = 0;
        currentMovementInputDirection.Normalize();
    }

    private void Move()
    {
        rBody.velocity = Vector3.Lerp(rBody.velocity, currentMovementInputDirection * movementSpeed, Time.deltaTime * 5);
        anima.SetFloat("Velocity", rBody.velocity.magnitude);

        Vector3 direction = rBody.velocity;
        direction.y = 0;
        direction.Normalize();

        if (rBody.velocity.magnitude > 0.1f)
            model.rotation = Quaternion.LookRotation(direction, Vector3.up);
    }
}
