﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // referenses
    public Transform cameraRigY;
    public Transform cameraRigX;
    public Transform targetObject;

    // parameters
    public float minX, maxX;
    public float xSensitivity;
    public float ySensitivity;
    public float followSpeed;

    public bool invertX;

    // cache
    private float xInput;
    private float currentXRotation;



    private void Update()
    {
        ProcessInput();
    }

    private void FixedUpdate()
    {
        cameraRigY.position = Vector3.Lerp(cameraRigY.position, targetObject.position, followSpeed * Time.fixedDeltaTime);
        RotateCamera();
    }

    private void ProcessInput()
    {
        xInput = Input.GetAxis("Mouse X");
        currentXRotation += -Input.GetAxis("Mouse Y") * ySensitivity * Time.deltaTime;
        currentXRotation = Mathf.Clamp(currentXRotation, minX, maxX);
    }

    private void RotateCamera()
    {
        cameraRigX.localRotation = Quaternion.Euler(currentXRotation, 0, 0);
        cameraRigY.Rotate(0, xInput * xSensitivity * Time.fixedDeltaTime, 0, Space.Self);
    }
}