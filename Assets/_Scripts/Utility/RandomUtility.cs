﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomUtility
{
    public static T GetRandomObject<T>(T[] objects)
    {
        int rnd = Random.Range(0, objects.Length);

        return objects[rnd];
    }

    public static T GetRandomObject<T>(List<T> objects)
    {
        int rnd = Random.Range(0, objects.Count);

        return objects[rnd];
    }

    public static int RarityDice(params float[] rarity)
    {
        float chance = Random.Range(0, 1f);

        for (int i = 0; i < rarity.Length; i++)
        {
            if (chance <= rarity[i])
                return i;
        }

        return -1;
    }
}
