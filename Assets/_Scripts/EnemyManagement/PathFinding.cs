﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour
{
    // referenses
    public PathRequestManager requestManager;
    public MapGenerator mapGen;



    public void StartFindPath(GridTile startPos, GridTile targetPos)
    {
        StartCoroutine(FindPath(startPos, targetPos));
    }

    IEnumerator FindPath(GridTile startTile, GridTile endTile)
    {
        Heap<GridTile> openSet = new Heap<GridTile>(mapGen.MaxGridSize);
        HashSet<GridTile> closedSet = new HashSet<GridTile>();
        openSet.Add(startTile);
        bool pathSuccess = false;

        GridTile[] wayPoints = new GridTile[0];

        while (openSet.Count > 0)
        {
            GridTile currentTile = openSet.RemoveFirst();

            closedSet.Add(currentTile);
            
            if(currentTile == endTile)
            {
                pathSuccess = true;
                break;
            }

            foreach (GridTile N in mapGen.GetNeightbours(currentTile))
            {
                if (N.Wall || closedSet.Contains(N))
                    continue;

                int newMovementCostToNeighbour = currentTile.gCost + GetDistance(currentTile, N);

                if(newMovementCostToNeighbour < N.gCost || !openSet.Contains(N))
                {
                    N.gCost = newMovementCostToNeighbour;
                    N.hCost = GetDistance(N, endTile);
                    N.pathParent = currentTile;

                    if(!openSet.Contains(N))
                    {
                        openSet.Add(N);
                    }
                    else
                    {
                        openSet.UpdateItem(N);
                    }
                }
            }
        }
        yield return null;

        if(pathSuccess)
        {
            wayPoints = RetracePath(startTile, endTile);
        }

        requestManager.FinishedProcessingPath(wayPoints, pathSuccess);
        Debug.Log("Finished processing path, returning path : " + wayPoints);
    }

    private GridTile[] RetracePath(GridTile startTile, GridTile endTile)
    {
        List<GridTile> path = new List<GridTile>();
        GridTile currentTile = endTile;
        while(currentTile != startTile)
        {
            path.Add(currentTile);
            currentTile = currentTile.pathParent;
        }

        path.Reverse();

        return path.ToArray();
    }

    private int GetDistance(GridTile A, GridTile B)
    {
        int dstX = Mathf.Abs(A.X - B.X);
        int dstY = Mathf.Abs(A.Y - B.Y);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        else
            return 14 * dstX + 10 * (dstY - dstX);
    }
}
