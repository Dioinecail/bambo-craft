﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TrapsManagement;

public class EnemyBehaviour : MonoBehaviour
{
    public event Action onDieEvent;
    public event Action<float> onHpChangedEvent;

    // referenses
    public Transform model;
    public Animator anima;

    // parameters
    public float movementSpeed;
    public float movementSpeedVariance;
    public float attackRadius;
    public LayerMask attackMask;

    private float hp;
    [SerializeField]
    private float maxHp;
    public float HP
    {
        get { return hp; }
        set
        {
            if (value >= maxHp)
                hp = maxHp;
            else if (value <= 0)
            {
                hp = 0;
                Die();
            }
            else
                hp = value;

            onHpChangedEvent?.Invoke(hp / maxHp);
        }
    }
    public int dropResources;

    // cache
    private List<GridTile> path;
    private int currentPathTile;
    private bool pathReady;
    private float initialMovementSpeed;
    private Coroutine coroutine_move;



    private void Awake()
    {
        float variance = UnityEngine.Random.Range(-movementSpeedVariance, movementSpeedVariance);
        float animatorVariance = 1 + (variance / movementSpeed);

        anima.speed = animatorVariance;
        movementSpeed += variance;
        initialMovementSpeed = movementSpeed;
        hp = maxHp;

        coroutine_move = StartCoroutine(MoveByPath());
    }

    public void RecieveDamage(float damage)
    {
        HP -= damage;
    }

    public void ResetMovementSpeed()
    {
        movementSpeed = initialMovementSpeed;
    }

    private void MoveTo(Vector3 position)
    {
        Vector3 direction = position - transform.position;
        direction.y = 0;
        model.rotation = Quaternion.Lerp(model.rotation, Quaternion.LookRotation(direction, Vector3.up), 3 * Time.deltaTime);

        transform.position = Vector3.MoveTowards(transform.position, position, movementSpeed * Time.deltaTime);
    }

    private void AttemptAttack()
    {
        if(Vector3.Distance(MainBaseBehaviour.Instance.transform.position, transform.position) < attackRadius)
        {
            MainBaseBehaviour.Instance.RecieveDamage();
            Destroy(gameObject);
        }
    }

    public void GetPath(GridTile startingTile, GridTile endTile)
    {
        PathRequestManager.RequestPath(startingTile, endTile, OnPathResult);
    }

    private Vector3 VaryPosition(Vector3 position)
    {
        position.x += UnityEngine.Random.Range(-0.25f, 0.25f) * MapGenerator.tilesScale;
        position.z += UnityEngine.Random.Range(-0.25f, 0.25f) * MapGenerator.tilesScale;
        return position;
    }

    private void OnPathResult(GridTile[] resultPath, bool success)
    {
        if (success)
        {
            pathReady = true;
            path = resultPath.ToList();
        }
        else
            Debug.LogError("Could not find any path for " + name);
    }

    private void Die()
    {
        onDieEvent?.Invoke();
        TrapsBuilder.AddResources(dropResources);
        Destroy(gameObject);
    }

    private IEnumerator MoveByPath()
    {
        if (pathReady)
        {
            Vector3 targetPosition = MapGenerator.GetPosition(path[currentPathTile]);
            targetPosition = VaryPosition(targetPosition);
            targetPosition.y = transform.position.y;

            while (Vector3.Distance(targetPosition, transform.position) > 0.1f)
            {
                MoveTo(targetPosition);
                yield return null;
            }

            currentPathTile++;
        }
        else
        {
            yield return new WaitForSeconds(1);

            yield return MoveByPath();
        }

        AttemptAttack();

        yield return MoveByPath();
    }
}