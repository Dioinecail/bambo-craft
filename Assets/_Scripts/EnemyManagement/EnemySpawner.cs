﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // referenses
    public Transform enemiesRoot;
    public MapGenerator mapGenerator;
    public EnemyBehaviour enemyPrefab;

    // parameters
    public float spawnDelay;
    public float spawnRateIncrementTime;
    public int currentSpawnIndex;

    // cache
    private List<GridTile> spawnTiles;



    public void StartSpawning()
    {
        FindSpawnPositions();

        StartCoroutine(SpawnMobs());
        StartCoroutine(IncrementSpawnIndex());
    }

    private void SpawnMob()
    {
        for (int i = 0; i <= currentSpawnIndex && i < spawnTiles.Count; i++)
        {
            Vector3 position = MapGenerator.GetPosition(spawnTiles[i]) + Vector3.up;

            EnemyBehaviour enemy = Instantiate(enemyPrefab, position, Quaternion.identity, enemiesRoot);
            enemy.GetPath(spawnTiles[i], GameManager.Instance.GetMainBaseTile());
        }
    }

    private void FindSpawnPositions()
    {
        spawnTiles = new List<GridTile>();

        for (int x = 0; x < mapGenerator.Map.GetLength(0); x++)
        {
            GridTile tile = mapGenerator.Map[x, 1];

            if (!tile.Wall)
                spawnTiles.Add(tile);

            tile = mapGenerator.Map[x, mapGenerator.Map.GetLength(1) - 2];

            if (!tile.Wall)
                spawnTiles.Add(tile);
        }

        for (int y = 0; y < mapGenerator.Map.GetLength(1); y++)
        {
            GridTile tile = mapGenerator.Map[1, y];

            if (!tile.Wall)
                spawnTiles.Add(tile);

            tile = mapGenerator.Map[y, mapGenerator.Map.GetLength(0) - 2];

            if (!tile.Wall)
                spawnTiles.Add(tile);
        }
    }

    private IEnumerator SpawnMobs()
    {
        yield return new WaitForSeconds(spawnDelay);

        SpawnMob();

        yield return SpawnMobs();
    }

    private IEnumerator IncrementSpawnIndex()
    {
        yield return new WaitForSeconds(spawnRateIncrementTime);

        currentSpawnIndex++;

        yield return IncrementSpawnIndex();
    }
}