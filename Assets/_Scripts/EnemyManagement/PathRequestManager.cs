﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PathRequestManager : MonoBehaviour
{
    // static
	public static PathRequestManager instance;

    // referenses
	public PathFinding pathfinding;

    // cache
	private bool isProcessingPath;
	private PathRequest currentPathRequest;
	private Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();



	private void Awake()
	{
		instance = this;
	}

	public static void RequestPath(GridTile pathStart, GridTile pathEnd, Action<GridTile[], bool> callback)
	{
		PathRequest newRequest = new PathRequest(pathStart, pathEnd, callback);
		instance.pathRequestQueue.Enqueue(newRequest);
		instance.TryProcessNext();
	}

	private void TryProcessNext()
	{
		if (!isProcessingPath && pathRequestQueue.Count > 0)
		{
			currentPathRequest = pathRequestQueue.Dequeue();
			isProcessingPath = true;
			pathfinding.StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd);
		}
	}

	public void FinishedProcessingPath(GridTile[] path, bool success)
	{
		currentPathRequest.callback(path, success);
		isProcessingPath = false;
		TryProcessNext();
	}

	private struct PathRequest
	{
		public GridTile pathStart;
		public GridTile pathEnd;
		public Action<GridTile[], bool> callback;

		public PathRequest(GridTile _start, GridTile _end, Action<GridTile[], bool> _callback)
		{
			pathStart = _start;
			pathEnd = _end;
			callback = _callback;
		}

	}
}