﻿using UnityEngine;
using TrapsManagement;
using UnityEngine.UI;
using TMPro;

public class TrapInfoDisplay : MonoBehaviour
{
    // referenses
    public Image trapImage;

    public Image iconTint;
    public TMP_Text textTint;

    public TMP_Text trapName;
    public TMP_Text trapDamage;
    public TMP_Text trapAttackSpeed;
    public TMP_Text trapDescription;
    public TMP_Text resourcesNumber;
    public TMP_Text costNumber;

    // parameters
    public Color availableColor;
    public Color unAvailableColor;

    // cache
    private TrapRecipe selectedRecipe;



    public void DisplayTrapStats(TrapRecipe recipe)
    {
        selectedRecipe = recipe;

        trapName.text = recipe.trapName;
        trapDamage.text = recipe.resultingTrap.baseDamage.ToString(":0.0");
        trapAttackSpeed.text = recipe.resultingTrap.baseAttackSpeed.ToString(":0.0");
        trapDescription.text = recipe.description;
        costNumber.text = recipe.cost.ToString(":000");

        trapImage.sprite = recipe.sprite;
        UpdateTrapDisplay(selectedRecipe.cost <= TrapsBuilder.availableResources);
    }

    public void UpdateTrapDisplay(bool available)
    {
        iconTint.color = textTint.color = available ? availableColor : unAvailableColor;
    }

    private void OnTrapSelected(TrapRecipe recipe)
    {
        DisplayTrapStats(recipe);
    }

    private void OnResourcesChanged(int amount)
    {
        resourcesNumber.text = amount.ToString(":000");

        if(selectedRecipe != null)
            UpdateTrapDisplay(selectedRecipe.cost <= amount);
    }

    private void OnEnable()
    {
        TrapsBuilder.onTrapSelected += OnTrapSelected;
        TrapsBuilder.onResourcesChanged += OnResourcesChanged;
    }

    private void OnDisable()
    {
        TrapsBuilder.onTrapSelected -= OnTrapSelected;
        TrapsBuilder.onResourcesChanged -= OnResourcesChanged;
    }
}