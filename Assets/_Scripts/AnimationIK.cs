﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationIK : MonoBehaviour
{
    // referenses
    public Transform cameraTransform;
    public Transform torsoBone;
    public Transform torsoRefBoneRoot;
    public Transform torsoRefBone;

    // parameters
    public float maxTorsoAngle;
    [Range(0, 1)]
    public float ikWeight;

    // cache
    private Vector3 initialDirection;



    private void Awake()
    {
        initialDirection = torsoBone.forward;
    }

    private void LateUpdate()
    {
        torsoRefBoneRoot.rotation = Quaternion.LookRotation(cameraTransform.forward, Vector3.up);
        torsoBone.rotation = Quaternion.Lerp(torsoBone.rotation, torsoRefBone.rotation, ikWeight);
    }
}